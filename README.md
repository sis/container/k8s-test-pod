# k8s test container

A container which just runs `sleep infinity`.

## Deploy

```sh
kubectl apply -f pod.yml
```

This will spawn a pod in the `test` namespace of your cluster.

## Connect to the pod

```sh
kubectl exec --namespace test -it pod/test-container -- /bin/ash
```

In the interactive shell you then can use e.g. `curl` to examine the
network.
